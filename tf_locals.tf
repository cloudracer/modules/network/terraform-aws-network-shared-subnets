locals {
  # var_enable_endpoints_parsed = [for endpoint in var.enable_endpoints : replace(replace(lower(endpoint), "-", ""), ".", "")]
  # var_security_groups         = compact([for endpoint in local.var_enable_endpoints_parsed : endpoint == "s3" || endpoint == "dynamodb" ? "" : endpoint])

  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-network-shared-subnets"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/network/terraform-aws-network-shared-subnets.git"
  }
  tags = merge(local.tags_module, var.tags)

  current_role_arn = join("/", [
    replace(split("/", data.aws_caller_identity.current.arn)[0], "assumed-role", "role"),
    split("/", data.aws_caller_identity.current.arn)[1]
    ]
  )

  network_role_arn = join("/", [
    replace(split("/", data.aws_caller_identity.network.arn)[0], "assumed-role", "role"),
    split("/", data.aws_caller_identity.network.arn)[1],
    ]
  )

}
