# VPC Settings #

## General
variable "main_vpc_name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
}

## Gateways
variable "use_igw" {
  description = "Controls if an Internet Gateway is created for public subnets and the related routes that connect them."
  type        = bool
  default     = false
}

variable "use_nat" {
  description = "Should be true if you want to provision NAT Gateways for each of your private networks"
  type        = bool
  default     = false
}

variable "tgw_routing" {
  description = "If TGW Routing should be used (only possible if TGW exists in that region)"
  type        = bool
  default     = false
}

## Subnets
variable "subnet_name" {
  type        = string
  description = "Name for the Subnets"
}

variable "subnets" {
  description = "A list of subnets inside the VPC"
  default     = []
}

// Endpoint Variables
variable "enable_endpoints" {
  description = "a list with endpoints need to be enabled - use the aws naming convention"
  default     = []
  type        = list(string)
}

variable "enable_ep_private_dns" {
  description = "enable private dns for vpc endpoints"
  default     = true
  type        = bool
}

## Other
variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}
