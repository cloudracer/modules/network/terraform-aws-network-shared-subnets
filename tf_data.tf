# General
data "aws_caller_identity" "current" {
  provider = aws
}

data "aws_region" "current" {
  provider = aws
}

data "aws_caller_identity" "network" {
  provider = aws.network
}

data "aws_region" "network" {
  provider = aws.network
}

# VPC

## VPC
data "aws_vpc" "main" {
  provider = aws.network

  filter {
    name   = "tag:Name"
    values = ["vpc-${var.main_vpc_name}-tflz"]
  }
}

## IGW
data "aws_internet_gateway" "main" {
  count    = var.use_igw ? 1 : 0
  provider = aws.network

  filter {
    name   = "attachment.vpc-id"
    values = [data.aws_vpc.main.id]
  }
}

## TGW
data "aws_ec2_transit_gateway" "main" {
  count    = var.tgw_routing ? 1 : 0
  provider = aws.network

  filter {
    name   = "tag:Name"
    values = ["tgw-network-${data.aws_region.network.name}-tflz"]
  }
}
