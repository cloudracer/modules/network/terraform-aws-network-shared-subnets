resource "aws_ram_resource_share" "this" {
  provider = aws.network

  name                      = "ram-${var.main_vpc_name}-${var.subnet_name}-tflz"
  allow_external_principals = false

  tags = local.tags
}

resource "aws_ram_principal_association" "this" {
  provider = aws.network

  principal          = data.aws_caller_identity.current.account_id
  resource_share_arn = aws_ram_resource_share.this.arn
}
