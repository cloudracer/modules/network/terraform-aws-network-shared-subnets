resource "null_resource" "tag_network_resources" {
  triggers = {
    manual = "iterate this number to trigger update: 3"
  }

  provisioner "local-exec" {
    when    = create
    command = <<-EOF
      python3 ${path.module}/scripts/tag-network-resources/tag_all.py -o ${local.network_role_arn} -t ${local.current_role_arn} -r ${data.aws_region.network.name}
    EOF
  }

  depends_on = [aws_ram_principal_association.this, module.vpc_subnets]
}
