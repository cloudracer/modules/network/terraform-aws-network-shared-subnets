# AWS VPC Module

## Overview

Creates a VPC with a dynamic number of Subnets and flexibile Subnet CIDR Ranges as well as NACLs, Routing and VPC Endpoints.

## Usage

````
module "vpc" {
  source = "./modules/vpc"

  name = local.general.name

  vpc_cidr           = "10.0.0.0/18"
  availability_zones = 3
  public_subnets     = ["10.0.0.0/22", "10.0.4.0/22", "10.0.8.0/22"]
  private_subnets    = ["10.0.16.0/22", "10.0.20.0/22", "10.0.24.0/22"]
  protected_subnets  = ["10.0.32.0/22", "10.0.36.0/22", "10.0.40.0/22"]

  create_vpc_internet_gateway = true
  create_vpc_nat_gateways     = true
  create_vpc_flow_logs        = true

  enable_endpoints = ["S3", "DynamoDB", "ssm", "ec2"]

  transit_gateway_id      = ""
  transit_gateway_routing = false

  tags = local.common_tags
}
````

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |
| <a name="provider_aws.network"></a> [aws.network](#provider\_aws.network) | 4.2.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.1.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_vpc_subnets"></a> [vpc\_subnets](#module\_vpc\_subnets) | ../terraform-aws-network-subnets | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_ram_principal_association.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ram_principal_association) | resource |
| [aws_ram_resource_share.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ram_resource_share) | resource |
| [null_resource.tag_network_resources](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_caller_identity.network](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_ec2_transit_gateway.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ec2_transit_gateway) | data source |
| [aws_internet_gateway.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/internet_gateway) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [aws_region.network](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [aws_vpc.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_enable_endpoints"></a> [enable\_endpoints](#input\_enable\_endpoints) | a list with endpoints need to be enabled - use the aws naming convention | `list(string)` | `[]` | no |
| <a name="input_enable_ep_private_dns"></a> [enable\_ep\_private\_dns](#input\_enable\_ep\_private\_dns) | enable private dns for vpc endpoints | `bool` | `true` | no |
| <a name="input_main_vpc_name"></a> [main\_vpc\_name](#input\_main\_vpc\_name) | Name to be used on all the resources as identifier | `string` | n/a | yes |
| <a name="input_subnet_name"></a> [subnet\_name](#input\_subnet\_name) | Name for the Subnets | `string` | n/a | yes |
| <a name="input_subnets"></a> [subnets](#input\_subnets) | A list of subnets inside the VPC | `list` | `[]` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | A map of tags to add to all resources | `map(string)` | `{}` | no |
| <a name="input_tgw_routing"></a> [tgw\_routing](#input\_tgw\_routing) | If TGW Routing should be used (only possible if TGW exists in that region) | `bool` | `false` | no |
| <a name="input_use_igw"></a> [use\_igw](#input\_use\_igw) | Controls if an Internet Gateway is created for public subnets and the related routes that connect them. | `bool` | `false` | no |
| <a name="input_use_nat"></a> [use\_nat](#input\_use\_nat) | Should be true if you want to provision NAT Gateways for each of your private networks | `bool` | `false` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
