locals {
  subnets_to_map = { for subnet in var.subnets : "${var.main_vpc_name}_${subnet.type}" => subnet } # Needed because TF can't work with a list of Maps in for_each
}

# For all Subnets that are private
module "vpc_subnets" {
  source   = "../terraform-aws-network-subnets"
  for_each = local.subnets_to_map
  providers = {
    aws = aws.network
  }

  # General info #
  vpc_id   = data.aws_vpc.main.id
  vpc_name = var.main_vpc_name

  # RAM Share #
  ram_share_arn = aws_ram_resource_share.this.arn

  # Gateway Info #
  route_to_igw            = var.use_igw
  igw_id                  = var.use_igw ? data.aws_internet_gateway.main[0].internet_gateway_id : ""
  create_vpc_nat_gateways = var.use_nat
  transit_gateway_routing = var.tgw_routing
  transit_gateway_id      = var.tgw_routing ? data.aws_ec2_transit_gateway.main[0].id : ""

  # Info from Subnet YAML #
  subnet_name           = var.subnet_name
  subnets_cidrs         = each.value.cidrs
  subnet_type           = each.value.type
  subnets_routes_to_tgw = lookup(each.value, "routes_to_tgw", [])
  dedicated_network_acl = lookup(each.value, "dedicated_network_acl", false) != false ? true : false
  inbound_acl_rules     = lookup(each.value, "dedicated_network_acl", false) != false ? lookup(each.value.dedicated_network_acl, "inbound_acl_rules", []) : []
  outbound_acl_rules    = lookup(each.value, "dedicated_network_acl", false) != false ? lookup(each.value.dedicated_network_acl, "outbound_acl_rules", []) : []
  enable_endpoints      = lookup(each.value, "endpoints", [])

  # Other #
  tags = local.tags
}
